# P2P Chat App

A simple Peer-to-Peer (P2P) chat application built using C# and Visual Studio.

## Overview

This P2P chat app allows users to communicate directly with each other in a decentralized manner. It leverages C# and Visual Studio for the implementation, providing a straightforward and user-friendly chat experience.

## Features

- **Peer-to-Peer Communication**: Establish direct communication channels between users, eliminating the need for a centralized server.
- **User-Friendly Interface**: Simple and intuitive UI for an easy chat experience.
- **C# and Visual Studio**: Built using the C# programming language and developed in Visual Studio for robust and efficient code.

## How to Use

1. **Clone the Repository:**
   ```bash
   git clone https://github.com/your-username/p2p-chat-app.git
   cd p2p-chat-app

2. **Open in Visual Studio:**
   Open the solution file (`P2PChatApp.sln`) in Visual Studio.

3. **Build and Run:**
   Build and run the application within Visual Studio. Ensure the necessary dependencies are installed.

4. **Connect with Peers:**
   Run multiple instances of the application to simulate different users. Input the necessary connection details to establish a P2P connection. USE 127.0.0.1 for local test

5. **Start Chatting:**
   Once connected, start chatting with other users in a decentralized and secure environment.

## Dependencies

- **C# 7.0 or higher**
- **Visual Studio 2019 or later**

## Contributions

Contributions are welcome! If you find any issues or want to enhance the functionality, feel free to open a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

Happy chatting!
