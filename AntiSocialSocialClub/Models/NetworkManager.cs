﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.ComponentModel;
using Newtonsoft.Json;
using System.Text.Json.Nodes;
using System.Runtime.InteropServices.JavaScript;
using System.Collections;
using System.Xml.Linq;
using System.Windows.Input;
using System.Windows;
using AntiSocialSocialClub.Views;
using Newtonsoft.Json.Linq;

namespace AntiSocialSocialClub.Models
{
    public class NetworkManager : INotifyPropertyChanged
    {
        private TcpClient client;
        private Stream stream;
        private TcpListener listener;
        private string message;
        private bool serverResponse;
        private bool continueConnection;
        private Dictionary<string, string> requestObject;
        private Dictionary<string, string> currentMessage;
        private bool serverRunning;
        private string chattingWith;
        private string username;
        private List<Dictionary<string, string>> currentHistory;

        public event PropertyChangedEventHandler? PropertyChanged;

        public Dictionary<string, string> RequestObject { get { return requestObject; } set { requestObject = value; OnPropertyChanged("RequestObject"); } }
        public string ChattingWith { get { return chattingWith; } set { chattingWith = value; OnPropertyChanged("ChattingWith"); } }
        public Dictionary<string, string> CurrentMessage { get { return currentMessage; } set { currentMessage = value; OnPropertyChanged("NewMessage"); } }
        
        public bool ServerRunning { get { return serverRunning; } set { serverRunning = value; } }

        public string Message { get { return message; } set { message = value; OnPropertyChanged("Message"); } }
        public bool ServerResponse { get { return serverResponse; } set { serverResponse = value; OnPropertyChanged("OpenClient"); } }
        public bool ContinueConnection { get { return continueConnection; } set { continueConnection = value; OnPropertyChanged("HandleRequest"); } }


        public NetworkManager()
        {
            this.requestObject = null;
        }

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        public bool StartClient(string name, string ipAddress, string portN)
        {
            bool connectionMade = false;
            username = name;
            Task clientTask = Task.Factory.StartNew(() =>
            {
                try
                {
                    IPAddress localaddr = IPAddress.Parse(ipAddress);
                    int port = int.Parse(portN);
                    IPEndPoint endPoint = new IPEndPoint(localaddr, port);

                    client = new TcpClient();

                    client.Connect(endPoint);
                    connectionMade = true;


                    stream = client.GetStream();
                    var requestData = new { request_type = "establishconnection", from = name, date = DateTime.Now };
                    string jsonRequest = JsonConvert.SerializeObject(requestData);
                    byte[] byteRequest = Encoding.ASCII.GetBytes(jsonRequest);
                    stream.Write(byteRequest, 0, byteRequest.Length);
                    while (true)
                    {
                        var buffer = new byte[1024];
                        var res = stream.Read(buffer, 0, 1024);
                        var message = Encoding.UTF8.GetString(buffer, 0, res);
                        var serverRes = JsonConvert.DeserializeObject<Dictionary<string, string>>(message);

                        if (serverRes != null)
                        {
                            System.Diagnostics.Debug.WriteLine("Response Received");


                            if (ValidRequest(serverRes))
                            {
                                System.Diagnostics.Debug.WriteLine("Response Valid");

                                if (serverRes.ContainsKey("status"))
                                {
                                    bool status = bool.Parse(serverRes["status"]);


                                    if (status)
                                    {
                                        if (Application.Current != null)
                                        {
                                            Application.Current.Dispatcher.InvokeAsync(() =>
                                            {
                                                OpenClient(status, serverRes);
                                            }).Wait();

                                        }
                                        HandleNetwork(serverRes);
                                    }
                                    else
                                    {
                                        ServerResponse = status;
                                    }
                                    break;
                                }


                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    connectionMade = false;
                    return;
                }

            });
            Task.Delay(1000).Wait();

            if (clientTask.Status == TaskStatus.Running && connectionMade)
            {
                return true;
            }

            return connectionMade;
        }
        public bool StartServer(string name, string ipAddress, string portN)
        {
            username = name;
            System.Diagnostics.Debug.WriteLine("Starting Server...");
            Task serverTask = Task.Factory.StartNew(() =>
            {
                try
                {
                    IPAddress localaddr = IPAddress.Parse(ipAddress);
                    int port = int.Parse(portN);
                    IPEndPoint endPoint = new IPEndPoint(localaddr, port);

                    listener = new TcpListener(endPoint);
                    listener.Start();

                    serverRunning = true;

                    //AWAIT FOR REQUEST !
                    while (serverRunning)
                    {
                        client = listener.AcceptTcpClient();
                        stream = client.GetStream();

                        var buffer = new byte[1024];
                        var res = stream.Read(buffer, 0, 1024);
                        var request = Encoding.UTF8.GetString(buffer, 0, res);

                        var decodedRequest = JsonConvert.DeserializeObject<Dictionary<string, string>>(request);

                        if (decodedRequest != null)
                        {
                            System.Diagnostics.Debug.WriteLine("Request Received!");
                            if (ValidRequest(decodedRequest))
                            {
                                Application.Current.Dispatcher.InvokeAsync(() =>
                                {
                                    RequestObject = decodedRequest;

                                });
                                System.Diagnostics.Debug.WriteLine("Request HANDLED");

                            }
                        }


                        Task.Factory.StartNew(() =>
                        {
                            while (true)
                            {
                                if (ContinueConnection)
                                {
                                    HandleNetwork(RequestObject);
                                    ContinueConnection = false;
                                    break;
                                }

                            }

                        });



                    }



                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    System.Diagnostics.Debug.WriteLine("TASK FAILED");
                }

            });

            Task.Delay(1000).Wait();

            if (serverTask.Status == TaskStatus.Running)
            {
                System.Diagnostics.Debug.WriteLine("TASK RUNNING LOOL");
                return true;
            }
            return false;
        }

        private void HandleNetwork(Dictionary<string, string> obj)
        {
            currentHistory = new List<Dictionary<string, string>>();
            System.Diagnostics.Debug.WriteLine("HANDLE THE NETWORK LOL");
            Application.Current.Dispatcher.Invoke(() =>
            {
                ChattingWith = obj["from"];
            });

            while (true)
            {
                try
                {
                    var buffer = new byte[1024];
                    var res = stream.Read(buffer, 0, 1024);
                    var message = Encoding.UTF8.GetString(buffer, 0, res);
                    var decodedMessage = JsonConvert.DeserializeObject<Dictionary<string, string>>(message);

                    if (decodedMessage != null)
                    {
                        if (decodedMessage["request_type"] == "disconnect")
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                WriteHistory();
                                OnPropertyChanged("ClientLost");

                            });
                            break;
                        }
                        if (decodedMessage["request_type"] == "message")
                        {
                            currentHistory.Add(decodedMessage);
                            CurrentMessage = decodedMessage;

                        }
                    }

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);


                }

            }


        }

        public void MessageDisconnect(string username)
        {
            try
            {
                if (stream != null)
                {
                    var response = new { request_type = "disconnect", from = username, date = DateTime.Now };

                    string jsonResponse = JsonConvert.SerializeObject(response);
                    byte[] responseBytes = Encoding.UTF8.GetBytes(jsonResponse);
                    stream.Write(responseBytes);

                }


            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("ERROR!LOL");
                System.Diagnostics.Debug.WriteLine(e);


            }

        }

        public void SendMessage(string messageString, string username)
        {
            _ = Task.Factory.StartNew(() =>
            {
                try
                {
                    var messageJSON = new { request_type = "message", from = username, message = messageString, date = DateTime.Now };


                    string message = JsonConvert.SerializeObject(messageJSON);

                    byte[] messageBytes = Encoding.UTF8.GetBytes(message);
                    stream.Write(messageBytes);
                    System.Diagnostics.Debug.WriteLine("MESSAGE SENT");
                    CurrentMessage = JsonConvert.DeserializeObject<Dictionary<string, string>>(message);
                    currentHistory.Add(CurrentMessage);
                }
                catch (Exception ex)
                {

                }

            });
        }
        public Tuple<bool, string> VerifyUserInput(string username, string ipAddress, string portN)
        {
            if (username == null || ipAddress == null || portN == null)
            {
                return new Tuple<bool, string>(false, "Please fill all fields!");
            }
            Match checkUsername = Regex.Match(username, @"^[a-zA-Z]+$");
            if (!checkUsername.Success)
            {
                return new Tuple<bool, string>(false, "Invalid username. Must contain only letters!");
            }

            Match checkIPAddress = Regex.Match(ipAddress, @"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$");
            if (!checkIPAddress.Success)
            {
                return new Tuple<bool, string>(false, "Invalid IP address!");
            }

            try
            {
                int port = int.Parse(portN);
                if (port > 65535 || port < 1)
                {
                    return new Tuple<bool, string>(false, "Invalid port number");

                }

            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, "Invalid port number");

            }
            return new Tuple<bool, string>(true, "");

        }

        private bool ValidRequest(Dictionary<string, string> request)
        {
            if (request["request_type"] == "establishconnection")
            {
                return true;
            }
            return false;


        }
        public void WriteHistory()
        {
            try
            {
                string json = File.ReadAllText("../DB/History.json");

                Dictionary<string, Dictionary<string, List<Dictionary<string, string>>>> items = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, List<Dictionary<string, string>>>>>(json);

                if (items != null && currentHistory.Count != 0)
                {
                    if (items.ContainsKey(username))
                    {
                        string key = ChattingWith +" "+ currentHistory[0]["date"];
                        items[username].Add(key, currentHistory);

                    }
                    else
                    {
                        Dictionary<string, List<Dictionary<string, string>>> newHistory = new Dictionary<string, List<Dictionary<string, string>>>();
                        string key = ChattingWith + " " + currentHistory[0]["date"];

                        newHistory.Add(key, currentHistory);

                        items.Add(username, newHistory);
                    }
                    string output = Newtonsoft.Json.JsonConvert.SerializeObject(items, Newtonsoft.Json.Formatting.Indented);
                    File.WriteAllText("../DB/History.json", output);

                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                System.Diagnostics.Debug.WriteLine("ERROR ON SAVE");
            }


        }
        public void HandleRequest(bool resType, string username)
        {
            ContinueConnection = resType;
            var response = new { request_type = "establishconnection", from = username, status = resType, date = DateTime.Now };
            string jsonResponse = JsonConvert.SerializeObject(response);
            byte[] responseBytes = Encoding.UTF8.GetBytes(jsonResponse);
            stream.Write(responseBytes);

        }
        private void OpenClient(bool status, Dictionary<string, string> obj)
        {
            ServerResponse = status;

        }

        public void CloseServer()
        {
            serverRunning = false;
            listener.Stop();
        }

        public Dictionary<string, List<Dictionary<string,string>>> GetHistory(string username)
        {
            string json = File.ReadAllText("../DB/History.json");

            Dictionary<string, Dictionary<string, List<Dictionary<string, string>>>> items = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, List<Dictionary<string, string>>>>>(json);

            if (items.ContainsKey(username))
            {
                return items[username];
            }

            return null;
        }



    }


}
