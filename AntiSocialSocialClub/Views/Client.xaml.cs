﻿using AntiSocialSocialClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AntiSocialSocialClub.ViewModels;

namespace AntiSocialSocialClub.Views
{
    /// <summary>
    /// Interaction logic for Client.xaml
    /// </summary>
    public partial class Client : Window
    {
        public Client(NetworkManager model, string username)
        {
            InitializeComponent();
            this.DataContext = new ClientViewModel(model, username);

        }
    }
}
