﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.JavaScript;
using System.Text;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AntiSocialSocialClub.Models;
using AntiSocialSocialClub.ViewModels.Commands;
using AntiSocialSocialClub.Views;
using Newtonsoft.Json;
namespace AntiSocialSocialClub.ViewModels
{
    class ClientViewModel : INotifyPropertyChanged
    {
        private NetworkManager networkManager;
        private string newMessage;
        private string username;
        private string clientName;
        private string hideSendEquipment;
        private Dictionary<string, string> requestObject;
        private string incommingRequest;
        private string requestName;
        private string querySearch;
        private ICommand querySearchC;
        private ICommand acceptRequestC;
        private ICommand declineRequestC;
        private ICommand sendMessageC;

        private string currentMessage;

        private ObservableCollection<HistoryItem> historyList;

        public string QuerySearch { set { querySearch = value; OnPropertyChanged(nameof(QuerySearch)); } get { return querySearch; } }

        public ObservableCollection<HistoryItem> HistoryList
        {
            get { return historyList; }
            set
            {
                if (historyList != value)
                {
                    historyList = value;
                    OnPropertyChanged(nameof(HistoryList));
                }
            }

        }
        private ObservableCollection<DictionaryItem> messages;

        public ObservableCollection<DictionaryItem> Messages
        {
            get { return messages; }
            set
            {
                if (messages != value)
                {
                    messages = value;
                    OnPropertyChanged(nameof(Messages));
                }
            }
        }

        public string CurrentMessage { get { return currentMessage; } set { currentMessage = value; } }
        public string RequestName { set { requestName = value; OnPropertyChanged(nameof(RequestName)); } get { return requestName; } }
        public string ClientName { set { clientName = value; OnPropertyChanged(nameof(ClientName)); } get { return clientName; } }
        public string IncommingRequest { set { incommingRequest = value; OnPropertyChanged(nameof(IncommingRequest)); } get { return incommingRequest; } }
        public string HideSendEquipment { set { hideSendEquipment = value; OnPropertyChanged(nameof(HideSendEquipment)); } get { return hideSendEquipment; } }
        public Dictionary<string, string> RequestObject { set { requestObject = value; } get { return requestObject; } }
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public ICommand AcceptRequestC
        {
            get
            {
                if (acceptRequestC == null)
                {
                    acceptRequestC = new HandleRequestCommand(this, true);
                }
                return acceptRequestC;
            }
            set { acceptRequestC = value; }
        }

        public ICommand SendMessageC
        {
            get
            {
                if (sendMessageC == null)
                {
                    sendMessageC = new SendMessageCommand(this);
                }
                return sendMessageC;
            }
            set { sendMessageC = value; }
        }
        public ICommand DeclineRequestC
        {
            get
            {
                if (declineRequestC == null)
                {
                    declineRequestC = new HandleRequestCommand(this, false);
                }
                return declineRequestC;
            }
            set { declineRequestC = value; }
        }
        public ICommand QuerySearchC {
            get
            { 
                if(querySearchC == null) 
                {
                    querySearchC = new QuerySearchCommand(this);
                }
                return querySearchC; 
            } 
            set { querySearchC = value; }
        }
        public ClientViewModel(NetworkManager manager, string username) 
        {
            this.networkManager = manager;
            Username = username;
            networkManager.PropertyChanged += networkManager_PropertyChanged;
            IncommingRequest = "Hidden";
            HideSendEquipment = "Hidden";
            Messages = new ObservableCollection<DictionaryItem>();
            HistoryList = new ObservableCollection<HistoryItem>();

            GetHistory(username);


        }

        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        private void networkManager_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Message")
            {                                                                                                                         
                newMessage = networkManager.Message;
            }
            if(e.PropertyName == "HandleRequest")
            {
                HideRequest();
            }
            if(e.PropertyName == "RequestObject")
            {
                requestObject = networkManager.RequestObject;
                ShowRequest();
            }
            if( e.PropertyName == "ChattingWith")
            {
                System.Diagnostics.Debug.WriteLine("CHAT CONNECTED LOL");

                ClientName = networkManager.ChattingWith + " has connected !;D";
                HideSendEquipment = "Visible";
                Messages.Clear();
            }
            if(e.PropertyName == "ClientLost")
            {
                HideSendEquipment = "Hidden";
                ClientName = networkManager.ChattingWith + " has disconnected! ;(";
                Messages.Clear();
                networkManager.WriteHistory();
                HistoryList.Clear();
                GetHistory(username);
            }
            if(e.PropertyName == "NewMessage")
            {
                System.Diagnostics.Debug.WriteLine("NOTIFY NEW MESSAGE!");

                AppendMessage(networkManager.CurrentMessage);
            }
        }

    
        public void SendMessage()
        {
            System.Diagnostics.Debug.WriteLine("SENDING MESSAGE!");

            networkManager.SendMessage(CurrentMessage, Username);
        }

        private void ShowRequest()
        {
            if (requestObject != null)
            {
                RequestName = requestObject["from"];
                IncommingRequest = "Visible";
            }
        }
        private void HideRequest()
        {
            RequestName = "";
            IncommingRequest = "Hidden";
        }
        public void HandleRequest(bool resType)
        {
            System.Diagnostics.Debug.WriteLine("BUTTON PRESSED");

            networkManager.HandleRequest(resType, Username);
        }
        private void AppendMessage(Dictionary<string, string> message)
        {
            if (message.TryGetValue("message", out string text) && message.TryGetValue("date", out string time) && message.TryGetValue("from", out string author))
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    Messages.Add(new DictionaryItem { Date = time, Message = text, Author = author });
                });
            }
        }

        public void MakeQuery()
        {
            Dictionary<string, List<Dictionary<string, string>>> historyResult = networkManager.GetHistory(username);
            ObservableCollection<HistoryItem> temp = new ObservableCollection<HistoryItem>();
            foreach (var item in historyResult)
            {
                HistoryItem historyItem = new HistoryItem();
                historyItem.Name = item.Key.Split(" ")[0];
                historyItem.Date = item.Value[0]["date"];
                historyItem.Command = new ShowHistoryCommand(this, item.Value);
                temp.Add(historyItem);

            }


            if (!string.IsNullOrEmpty(QuerySearch))
            {

                var queryResult = temp
                    .Where(item => item.Name.Contains(QuerySearch, StringComparison.OrdinalIgnoreCase))
                    .ToList();

                List<HistoryItem> filteredList = new List<HistoryItem>(queryResult);

                HistoryList.Clear();

                foreach (HistoryItem item in filteredList)
                {
                    System.Diagnostics.Debug.Print(item.Name);
                    HistoryList.Add(item);
                }
            }
            else
            {
                HistoryList.Clear();
                GetHistory(username);
            }
        }


        private void GetHistory(string username)
        {
            Dictionary<string, List<Dictionary<string, string>>> historyResult = networkManager.GetHistory(username);
            if(historyResult != null)
            {
                foreach(var item in historyResult.Reverse()) 
                {
                    HistoryItem historyItem = new HistoryItem();
                    historyItem.Name = item.Key.Split(" ")[0];
                    historyItem.Date = item.Value[0]["date"];
                    historyItem.Command = new ShowHistoryCommand(this, item.Value);
                    HistoryList.Add(historyItem);

                }
            }
        }
        public void ShowHistory(List<Dictionary<string, string>> history)
        {
            Messages.Clear();
            networkManager.MessageDisconnect(username);
            hideSendEquipment = "Hidden";
            if (networkManager.ChattingWith != null)
            {
                ClientName = networkManager.ChattingWith + " has disconnected! ;(";
            }

            foreach (var item in history)
            {
                DictionaryItem newItem = new DictionaryItem();
                newItem.Date = item["date"];
                newItem.Message = item["message"];
                newItem.Author = item["from"];

                Messages.Add(newItem);

            }

        }
    }
}

public class DictionaryItem
{
    public string Date { get; set; }
    public string Message { get; set; }
    public string Author { get; set; }
}

public struct HistoryItem
{
    public string Date { get; set; }
    public string Name { get; set; }

    public string FullString { get { return Name + "\n" + Date; } set { FullString = value; } }

    public ICommand Command { get; set; }
    
}