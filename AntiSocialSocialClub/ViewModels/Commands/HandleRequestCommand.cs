﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AntiSocialSocialClub.ViewModels.Commands
{
    internal class HandleRequestCommand : ICommand
    {
        public event EventHandler? CanExecuteChanged;
        private ClientViewModel parent;
        private bool resType;

        public HandleRequestCommand(ClientViewModel parent, bool resType)
        {
            this.parent = parent;
            this.resType = resType;

        }
        

        public bool CanExecute(object? parameter)
        {
            return true;
        }

        public void Execute(object? parameter)
        {
            parent.HandleRequest(resType);
        }
    }
}
