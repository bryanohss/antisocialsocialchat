﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AntiSocialSocialClub.ViewModels.Commands
{
    internal class ConnectToServerCommand : ICommand
    {
        public event EventHandler? CanExecuteChanged;

        public MainWindowViewModel parent;

        public ConnectToServerCommand(MainWindowViewModel parent)
        {
            this.parent = parent;
        }

        public bool CanExecute(object? parameter)
        {
            return true;
        }

        public void Execute(object? parameter)
        {
            parent.ConnectToServer();
        }
    }
}
