﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AntiSocialSocialClub.ViewModels.Commands
{
    internal class QuerySearchCommand : ICommand
    {
        private ClientViewModel parent;

        public QuerySearchCommand(ClientViewModel parent)
        {
            this.parent = parent;
        }
        public event EventHandler? CanExecuteChanged;

        public bool CanExecute(object? parameter)
        {
            return true;
        }

        public void Execute(object? parameter)
        {
            parent.MakeQuery();
        }
    }
}
