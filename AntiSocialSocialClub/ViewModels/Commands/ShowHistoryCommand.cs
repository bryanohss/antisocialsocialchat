﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AntiSocialSocialClub.ViewModels.Commands
{
    internal class ShowHistoryCommand : ICommand
    {
        private ClientViewModel parent;
        private List<Dictionary<string, string>> history;

        public ShowHistoryCommand(ClientViewModel parent, List<Dictionary<string, string>> history) 
        {
            this.parent = parent;
            this.history = history;
        }
        public event EventHandler? CanExecuteChanged;

        public bool CanExecute(object? parameter)
        {
            return true;
        }

        public void Execute(object? parameter)
        {
            parent.ShowHistory(history);
        }
    }
}
