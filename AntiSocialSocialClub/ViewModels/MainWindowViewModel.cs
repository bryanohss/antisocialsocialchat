﻿using AntiSocialSocialClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AntiSocialSocialClub.ViewModels.Commands;
using System.Windows.Input;
using System.Windows;
using System.Reflection.Metadata;
using System.ComponentModel;
using AntiSocialSocialClub.Views;
namespace AntiSocialSocialClub.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private string username;
        private string ipAddress;
        private string portN;
        private string errorMessage;
        private ICommand startServerC;
        private ICommand connectToServerC;
       
        private NetworkManager networkManager;

        public event PropertyChangedEventHandler? PropertyChanged;
        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string ErrorMessage { get { return errorMessage; } set { errorMessage = value; OnPropertyChange(nameof(ErrorMessage)); } }
        public string UserName { get { return username; } set { username = value; } }
        public string IPAddress { get { return ipAddress; } set { ipAddress = value; } }
        public string PortN { get { return portN; } set { portN = value; } }
        public NetworkManager NetworkManager
        {
            set { networkManager = value; }
            get { return networkManager; }
        }
      
     
        public ICommand StartServerC
        {
            get
            {
                if(startServerC == null)
                {
                    startServerC = new StartServerCommand(this);
                }
                return startServerC;

            }
            set { startServerC = value; }
        }

        public ICommand ConnectToServerC
        {
            get
            {
                if(connectToServerC == null)
                {
                    connectToServerC = new ConnectToServerCommand(this);
                }
                return connectToServerC;
            }

            set { connectToServerC = value; }
        }
        public MainWindowViewModel(NetworkManager networkMangager)
        {
            NetworkManager = networkMangager;
            errorMessage = String.Empty;
            networkManager.PropertyChanged += networkManager_PropertyChanged;

        }
        private void networkManager_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "OpenClient")
            {
                openClient();
            }
            
        }

        private void openClient()
        {
            System.Diagnostics.Debug.WriteLine("OPEN CLIENT FUNC");

            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                if (networkManager.ServerResponse)
                {
                    Application.Current.MainWindow.Hide();

                    Client client = new Client(networkManager, UserName);
                    client.ShowDialog();
                    networkManager.WriteHistory();
                    System.Diagnostics.Debug.WriteLine("SEND DISCONNECT");
                    networkManager.MessageDisconnect(username);
                    
                    Application.Current.MainWindow.Show();

                }
                else
                {
                    ErrorMessage = "User declined your request!";
                }
            });
        }

        public void startServer()
        {
            System.Diagnostics.Debug.WriteLine("Calling Model");
            Tuple<bool, string> verifyResult = networkManager.VerifyUserInput(UserName, IPAddress, PortN);
            if (verifyResult.Item1) {
                ErrorMessage = "";
                if (networkManager.StartServer(UserName, IPAddress, PortN))
                {

                    System.Diagnostics.Debug.WriteLine("Server Started!");

                    Application.Current.Dispatcher.InvokeAsync(() =>
                    {
                        Application.Current.MainWindow.Hide();
                        Client client = new Client(networkManager, UserName);
                        client.ShowDialog();

                        networkManager.MessageDisconnect(username);
                        networkManager.CloseServer();
                        Application.Current.MainWindow.Show();


                    });
                   
                }
                else
                {
                    ErrorMessage = "There is a user using this port!";
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(verifyResult.Item2);
                ErrorMessage = verifyResult.Item2;
            }
            

        }

        public void ConnectToServer()
        {
            Tuple<bool, string> verifyResult = networkManager.VerifyUserInput(UserName, IPAddress, PortN);
            if(verifyResult.Item1)
            {
                if(NetworkManager.StartClient(UserName, IPAddress, PortN)){
                    ErrorMessage = "Waiting for response";
                    System.Diagnostics.Debug.WriteLine("WAITING!");
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("NO ENDPOINT");
                    ErrorMessage = "There is no user listening to this port";
                }
                
                
            }
        }


    }
}
